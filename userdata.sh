dnf upgrade -y
timedatectl set-timezone "Europe/Bucharest"
hostnamectl set-hostname YOUR_DESIRED_HOSTNAME --static --pretty --transient
sed -i '/^PermitRootLogin/s/yes/no/' /etc/ssh/sshd_config
sed -i  's/PasswordAuthentication yes/PasswordAuthentication no/g' /etc/ssh/sshd_config
systemctl restart sshd
adduser YOUR_USERNAME
# Change the following password after first login!!!
echo 'ChangeMe'  | passwd --stdin YOUR_USERNAME
usermod -aG wheel YOUR_USERNAME
mkdir -p /home/YOUR_USERNAME/.ssh
chmod 700 /home/YOUR_USERNAME/.ssh
cp -a /root/.ssh/authorized_keys /home/YOUR_USERNAME/.ssh/authorized_keys && rm -f /root/.ssh/authorized_keys
chown -R YOUR_USERNAME:YOUR_USERNAME /home/YOUR_USERNAME
dnf install -y wget
wget http://software.virtualmin.com/gpl/scripts/install.sh -v -O install.sh; bash install.sh -f


# set a root password on the server; the credentials for the newly installed Virtualmin is the pair root/new_set_password
#https://www.virtualmin.com/node/52608