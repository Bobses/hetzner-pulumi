import * as pulumi from "@pulumi/pulumi";
import * as hcloud from "@pulumi/hcloud";
import { output } from "@pulumi/pulumi";
import * as fs from "fs";
import { SshKey } from "@pulumi/hcloud";


// Create a new SSH key
const sshKey = new hcloud.SshKey("my_key",
  {
    name: "my-key",
    publicKey: fs.readFileSync("ABSOLUTE_PATH_TO_SSH_KEY/id_rsa.pub").toString(),
    //the following line works too (if you choose to add the content of the public ssh key):
    //publicKey: "ssh-rsa ZZZuiaC1[...nl8nPnW1[...]yDUYPOfwQ[...]"
  });


const server1 = new hcloud.Server("node01", {
  image: "centos-8",
  name: "plm-centos8",
  serverType: "cx11",
  datacenter: "nbg1-dc3",
  sshKeys: [sshKey.name],
  userData: fs.readFileSync("./userdata.sh").toString(),
},
{
  dependsOn: [sshKey]
  },
);  

export const publicIP = server1.ipv4Address;